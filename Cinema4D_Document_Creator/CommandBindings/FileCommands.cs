﻿using System.Windows.Input;

namespace Cinema4DDocumentCreator.CommandBindings
{
    public class FileCommands
    {
        static FileCommands()
        {
            SaveAll = new RoutedUICommand(
                "S_aveAll", "Save All", typeof(FileCommands),
                new InputGestureCollection{ new KeyGesture(Key.S, ModifierKeys.Control | ModifierKeys.Shift, "Ctrl+Shift+S")});
        }
        public static RoutedUICommand SaveAll { get; }
    }
}
