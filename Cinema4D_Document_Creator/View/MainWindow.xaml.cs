﻿using System.Windows;
using System.Windows.Input;

namespace Cinema4DDocumentCreator.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Close_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true; //< --Set this to true to enable bindings.
        }

        private void New_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void Save_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            
        }

        private void SaveAll_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            
        }
    }
}
