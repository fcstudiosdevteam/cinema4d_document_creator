﻿using System.Windows.Controls;

namespace Cinema4DDocumentCreator.Model.UIElement
{
    public class CustomTab : TabItem
    {
        /// <summary>
        /// Boolean to state if the tab has content has been edited
        /// </summary>
        private bool _contentEdited;

        public DocumentData Document { get; set; }


        public void UpdateLabel(bool value)
        {
            var header = Header.ToString();

            var result = header.Substring(header.Length - 1);

            if(!value)
            {
                Header = header.Remove(header.Length - 1, 1);
                _contentEdited = false;
                return;
            }

            if (!result.Equals("*") && value)
            {
                Header = $"{Header}*";
                _contentEdited = true;
            }
        }

        public bool IsEdited()
        {
            return _contentEdited;
        }
    }
}
