﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using Cinema4DDocumentCreator.Model.UIElement;
using Cinema4DDocumentCreator.View;

namespace Cinema4DDocumentCreator.Model
{
    public static class CoreData
    {
        public static CustomTab SelectedTab { get; set; }

        public static ObservableCollection<CustomTab> Tabs { get; set; } = new ObservableCollection<CustomTab>();

        internal static void CloseTab(CustomTab tabItem)
        {
            if (tabItem.IsEdited())
            {
                var result =
                    MessageBox.Show(
                        "Changes where made! Would you like to save before closing?",
                        "Mod Not Saved", MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);

                switch (result)
                {
                    case MessageBoxResult.Cancel:
                        return;
                    case MessageBoxResult.Yes:
                        tabItem.Document.SaveCommandMethod();
                        break;
                }
            }

            CloseTabOperation(tabItem);
        }

        private static void CloseTabOperation(CustomTab tabItem)
        {
            //Unsub from watcher
            tabItem.Document.UnSubscribe();

            //Remove tab
            Tabs.Remove(tabItem);
        }

        public static void OpenNewTab(DocumentData value)
        {
            //See if tab is open.
            var match = Tabs.SingleOrDefault(x => x.Document.DocumentName == value.DocumentName);

            if (match != null)
            {
                match.IsSelected = true;
                return;
            }

            //Create a new mod page
            var modPage = new DocumentPageView();

            var tab = new CustomTab
            {
                Header = Path.GetFileName(value.DocumentName),
                Document = value,
                IsSelected = true,
                Content = modPage
            };

            value.Tab = tab;

            //Add new tab
            Tabs.Add(tab);
        }

    }
}
