﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cinema4DDocumentCreator.Model.UIElement;

namespace Cinema4DDocumentCreator.Model
{
    public class DocumentData
    {
        private FileSystemWatcher _watcher;
        public string DocumentPath { get; set; }
        public CustomTab Tab { get; set; }
        public string DocumentName { get; set; }

        private void Watch()
        {
            _watcher = new FileSystemWatcher();
            _watcher.Path = DocumentPath;
            _watcher.NotifyFilter = NotifyFilters.LastAccess
                                    | NotifyFilters.LastWrite
                                    | NotifyFilters.FileName
                                    | NotifyFilters.Size
                                    | NotifyFilters.DirectoryName;
            _watcher.Filter = "*.*";
            _watcher.Changed += OnChanged;
            _watcher.Created += OnChanged;
            _watcher.Deleted += OnChanged;
            _watcher.Renamed += OnChanged;
            _watcher.EnableRaisingEvents = true;
        }



        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            App.Current.Dispatcher.Invoke(UpdateFiles);
        }

        public void UpdateFiles()
        {
           
        }

        public void UnSubscribe()
        {
            if (_watcher == null) return;

            _watcher.Changed -= OnChanged;
            _watcher.Created -= OnChanged;
            _watcher.Deleted -= OnChanged;
            _watcher.Renamed -= OnChanged;
        }

        public void SaveCommandMethod()
        {
            
        }
    }
}
