﻿using System.Windows.Input;
using Cinema4DDocumentCreator.Model;
using Cinema4DDocumentCreator.ViewModel.Base;

namespace Cinema4DDocumentCreator.ViewModel.Pages
{
    public class MainWindowViewModel : BaseViewModel
    {
        public ICommand CreateNewDocumentCommand {get;set; }

        public MainWindowViewModel()
        {
            CreateNewDocumentCommand = new RelayCommand(CreateNewDocumentCommandMethod);
        }

        private void CreateNewDocumentCommandMethod()
        {
            var n = CoreData.Tabs.Count;
            CoreData.OpenNewTab(new DocumentData
            {
                DocumentName =$"New Document [{n}].cdc",
                DocumentPath = $@"C:/New Document {n}.cdc"
            });
        }
    }
}
