﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Markdig;

namespace Cinema4DDocumentCreator.ViewModel.Pages
{
    public class DocumentPageViewModel : INotifyPropertyChanged
    {
        private string _documentEditorText;
        public ICommand OnClick { get; set; }
        public string DocumentEditorText
        {
            get { return _documentEditorText; }
            set
            {
                _documentEditorText = value;
                CreateHtml(value);
            }
        }

        private string _htmlString;

        public string HTMLString
        {
            get => _htmlString;
            set
            {
                _htmlString = value; 
                OnPropertyChanged(nameof(HTMLString));
            }
        }


        private void CreateHtml(string value)
        {
            var result = Markdown.ToHtml(value);
            HTMLString = result;
            Console.WriteLine(result);
        }

        public DocumentPageViewModel()
        {
            DocumentEditorText = @"This is a text with some *emphasis*";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
